package alexander.user.controller;

import alexander.user.model.User;
import alexander.user.model.UserProfile;
import alexander.user.service.UserProfileService;
import alexander.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/")
public class AppController {

    @Autowired
    UserService userService;

    @Autowired
    UserProfileService userProfileService;


    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
    public @ResponseBody List<User> listUsers() {

        List<User> users = userService.findAllUsers();
        return users;
    }

    @RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
    public void saveUser(@Valid @RequestBody User user, HttpServletResponse response) {
        userService.saveUser(user);
        response.setStatus(200);
    }

    @RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.GET)
    public @ResponseBody User editUser(@PathVariable Integer id) {
        User user = userService.findById(id);
        return user;
    }

    @RequestMapping(value = { "/edit-user" }, method = RequestMethod.POST)
    public void updateUser(@Valid @RequestBody User user, HttpServletResponse response) {
        userService.updateUser(user);
        response.setStatus(200);
    }

    @RequestMapping(value = { "/delete-user-{id}" }, method = RequestMethod.GET)
    public void deleteUser(@PathVariable Integer id, HttpServletResponse response) {
        userService.deleteUserById(id);
        response.setStatus(200);
    }


    @RequestMapping(value = "roles", method = RequestMethod.POST)
    public List<UserProfile> initializeProfiles() {
        return userProfileService.findAll();
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Exception handleAllException(Exception ex){
        return ex;
    }

}