package alexander.user.dao;

import alexander.user.model.User;

import java.util.List;

public interface UserDao {

    User findById(int id);

    User findByToken(String token);

    void save(User user);

    void deleteById(Integer id);

    List<User> findAllUsers();

}