package alexander.user.service;

import alexander.user.dao.UserDao;
import alexander.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ValidationException;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao dao;

    public User findById(int id) {
        return dao.findById(id);
    }

    public User findByToken(String token) {
        User user = dao.findByToken(token);
        return user;
    }

    public void saveUser(User user) {
        dao.save(user);
    }

    public void updateUser(User user) {
        validate(user);
        User entity = dao.findById(user.getId());
        if(entity!=null){
            entity.setToken(user.getToken());
            entity.setPassword(user.getPassword());
            entity.setLogin(user.getLogin());
            entity.setUserProfiles(user.getUserProfiles());
        }
    }

    private void validate(User user) {
        if(!isUserTokenUnique(user.getId(), user.getToken())){
            throw new ValidationException(String.format("User with token: %s already exist", user.getToken()));
        }
    }


    public void deleteUserById(Integer id) {
        dao.deleteById(id);
    }

    public List<User> findAllUsers() {
        return dao.findAllUsers();
    }

    public boolean isUserTokenUnique(Integer id, String token) {
        User user = findByToken(token);
        return ( user == null || ((id != null) && (user.getId() == id)));
    }

}