package alexander.user.service;

import alexander.user.model.User;

import java.util.List;

public interface UserService {

    User findById(int id);

    User findByToken(String token);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(Integer id);

    List<User> findAllUsers();

    boolean isUserTokenUnique(Integer id, String token);

}