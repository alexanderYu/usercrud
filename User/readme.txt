Разработать CRUD приложение используя технологии RESTful, SpringMVC совместно со Spring Security,
GUI не обязателен, но будет большим плюсом. Можно использовать SpringBoot.
Для этого нужно создать минимум одну таблицу в реляционной базе данных.


create table APP_USER (
   id BIGINT NOT NULL AUTO_INCREMENT,
   login VARCHAR(30) NOT NULL,
   password VARCHAR(100) NOT NULL,
   token VARCHAR(30),
   PRIMARY KEY (id),
   UNIQUE (token)
);
  
  
create table USER_PROFILE(
   id BIGINT NOT NULL AUTO_INCREMENT,
   type VARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (type)
);
  
  
CREATE TABLE APP_USER_USER_PROFILE (
    user_id BIGINT NOT NULL,
    user_profile_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, user_profile_id),
    CONSTRAINT FK_APP_USER FOREIGN KEY (user_id) REFERENCES APP_USER (id),
    CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES USER_PROFILE (id)
);
 
/* Populate USER_PROFILE Table */
INSERT INTO USER_PROFILE(type)
VALUES ('USER');
  
INSERT INTO USER_PROFILE(type)
VALUES ('ADMIN');

